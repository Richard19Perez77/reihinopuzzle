package audio;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import java.io.IOException;

import common.CommonVariables;
import data.Data;
import reihino.puzzleimage.R;

/**
 * A class to extend Media Player and implement handling interfaces. I also
 * started implementing the ability to handle the sound changes due to incoming
 * notification sounds like phone or message alerts *
 *
 * @author Rick
 */
public class MyMediaPlayer implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, AudioManager.OnAudioFocusChangeListener,
        MediaPlayer.OnCompletionListener {

    CommonVariables cv = CommonVariables.getInstance();

    public MediaPlayer mediaPlayer;
    Uri path = Uri.parse(cv.context.getString(R.string.PATH) + Data.TRACK_01);
    AudioManager am;
    int result;
    public float currentVolume = 0f;

    /**
     * Used in testing to tell if the headphones were unplugged or not
     */
    public boolean volumeSet = false;

    public enum State {
        Idle, Initialized, Prepared, Started, Preparing, Stopped, Paused, End, Error, PlaybackCompleted
    }

    public State currentState;

    public void init() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.setOnCompletionListener(this);
        }
        am = (AudioManager) cv.context.getSystemService(Context.AUDIO_SERVICE);
        mediaPlayer.reset();
        currentState = State.Idle;
    }

    public void start() {
        if (cv.playMusic)
            result = am.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                if (currentState != State.Idle && currentState != State.Preparing)
                    init();
                if (currentState != State.Preparing) try {
                    mediaPlayer.setDataSource(cv.context, path);
                    currentState = State.Initialized;
                    mediaPlayer
                            .setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setVolume(cv.volume, cv.volume);
                    currentVolume = cv.volume;
                    mediaPlayer.prepareAsync();
                    currentState = State.Preparing;
                } catch (IllegalArgumentException | IOException | SecurityException | IllegalStateException ignored) {
                }
            }
    }

    @Override
    public void onPrepared(MediaPlayer player) {
        // check for option to play music and resume last position
        if (currentState == State.Preparing) {
            currentState = State.Prepared;
            if (cv.playMusic) {
                if (cv.currentSoundPosition > 0) {
                    mediaPlayer.seekTo(cv.currentSoundPosition);
                }
                if (currentState != State.End && !player.isPlaying()) {
                    player.start();
                    currentState = State.Started;
                }
            }
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        currentState = State.Error;
        mediaPlayer.reset();
        start();
        return true;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        // Handle audio lowering and raising for other phone sounds
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume play back
                if (mediaPlayer == null)
                    init();
                else if (!mediaPlayer.isPlaying()) {
                    start();
                } else {
                    mediaPlayer.setVolume(cv.volume, cv.volume);
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                // lost focus for an unbounded amount of time. stop and release
                pause();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // lost focus for a short time, but we have to stop play back.
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    currentState = State.Paused;
                    cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                if (mediaPlayer != null) {
                    setNewVolume(0.1f);
                }
                break;
        }
    }

    public void resume() {
        init();
        start();
    }

    public void abandonFocus() {
        if (am != null)
            am.abandonAudioFocus(this);
    }

    public void pause() {
        abandonFocus();
        if (currentState != State.End && mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                currentState = State.Paused;
            }
            if (currentState == State.Started || currentState == State.Paused) {
                mediaPlayer.stop();
                currentState = State.Stopped;
            }
            mediaPlayer.release();
            currentState = State.End;
            mediaPlayer = null;
        }
    }

    public void setNewVolume(Float setVolume) {
        if (currentState != State.End && mediaPlayer.isPlaying()) {
            mediaPlayer.setVolume(setVolume, setVolume);
            currentVolume = setVolume;
            volumeSet = true;
        }
    }

    public void cleanUp() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            currentState = State.End;
            mediaPlayer = null;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        currentState = State.PlaybackCompleted;
        cv.currentSoundPosition = 0;
        start();
    }

    public void onStop() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                currentState = State.Paused;
                cv.currentSoundPosition = mediaPlayer.getCurrentPosition();
            }
        }
    }
}
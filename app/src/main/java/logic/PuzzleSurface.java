package logic;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.EditText;

import audio.MyMediaPlayer;
import common.CommonVariables;
import photo.SavePhoto;
import reihino.puzzleimage.R;

/**
 * A class to hold the surface of the puzzle and the thread for updating physics
 * and drawing.
 * <p/>
 * Currently no splash screen implemented.
 *
 * @author Rick
 */
public class PuzzleSurface extends SurfaceView implements
        SurfaceHolder.Callback {

    public static final int TRANS_VALUE = (255 / 2);
    public static final int STROKE_VALUE = 5;

    public Paint borderPaintA, borderPaintB, transPaint, fullPaint;

    public AdjustablePuzzle puzzle;
    public PuzzleUpdateAndDraw puzzleUpdateAndDraw;

    public MyMediaPlayer myMediaPlayer;

    public CommonVariables common = CommonVariables.getInstance();

    PuzzleSurface ps;

    private static final String TAG = "puzzleLog";
    public String defaultPuzzleSize= "7";

    public PuzzleSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (common.isLogging)
            Log.d(TAG, "constructor PuzzleSurface");
        // set context for access in other classes
        common.context = context;
        common.res = context.getResources();
        ps = this;

        // register our interest in hearing about changes to our surface
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        borderPaintA = new Paint();
        borderPaintA.setStyle(Paint.Style.STROKE);
        borderPaintA.setStrokeWidth(STROKE_VALUE);
        borderPaintA.setColor(Color.LTGRAY);
        borderPaintA.setAlpha(TRANS_VALUE);

        borderPaintB = new Paint();
        borderPaintB.setStyle(Paint.Style.STROKE);
        borderPaintB.setStrokeWidth(STROKE_VALUE);
        borderPaintB.setColor(Color.DKGRAY);
        borderPaintB.setAlpha(TRANS_VALUE);

        transPaint = new Paint();
        transPaint.setAlpha(TRANS_VALUE);
        transPaint.setStyle(Paint.Style.FILL);

        fullPaint = new Paint();

        // create thread only; it's started in surfaceCreated()
        puzzleUpdateAndDraw = new PuzzleUpdateAndDraw(holder, context);
    }

    public void nextImage() {
        synchronized (puzzleUpdateAndDraw.mSurfaceHolder) {
            if (common.isLogging)
                Log.d(TAG, "nextImage PuzzleSurface");

            AlertDialog.Builder builder;
            AlertDialog alert;

            builder = new AlertDialog.Builder(common.context);

            builder.setTitle("Save this image?");
            builder.setMessage("Solve time = " + puzzle.getSolveTime() + " secs.");
            builder.setPositiveButton("Yup",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new SavePhoto(common.currentPuzzleImagePosition);
                            common.hideButtons();
                            common.isImageLoaded = false;
                            puzzle.getNewImageLoadedScaledDivided();
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });
            builder.setNegativeButton("Nope",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            common.hideButtons();
                            common.isImageLoaded = false;
                            puzzle.getNewImageLoadedScaledDivided();
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });

            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // if the dialog is canceled give the option to start it
                    // again
                    if (dialog != null)
                        dialog.dismiss();
                }
            });

            alert = builder.create();
            alert.show();
        }
    }

    /**
     * Used on menu press to put thread into paused state and hided ui to prevent button press.
     *
     * @param hasWindowFocus if the window is in focus we can use touch and refresh the screen for drawing
     */
    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (common.isLogging)
            Log.d(TAG, "onWindowFocusChanged PuzzleSurface hasWindowFocus:" + hasWindowFocus);

        if (!hasWindowFocus) {
            common.isWindowInFocus = false;
        } else {
            common.isWindowInFocus = true;
            puzzleUpdateAndDraw.updateAndDraw();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        if (common.isLogging)
            Log.d(TAG, "surfaceChanged PuzzleSurface " + width + " " + height);

        puzzleUpdateAndDraw.surfaceChanged(width, height);
        if (common.resumePreviousPuzzle) {
            common.resumePreviousPuzzle = false;
            resumePuzzle();
        } else if (common.createNewPuzzle) {
            common.createNewPuzzle = false;
            createPuzzle();
        }
    }


    public void surfaceCreated(SurfaceHolder holder) {
        if (common.isLogging)
            Log.d(TAG, "surfaceCreated PuzzleSurface");

        puzzleUpdateAndDraw = new PuzzleUpdateAndDraw(holder, common.context);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (common.isLogging)
            Log.d(TAG, "surfaceDestroyed PuzzleSurface");
    }

    @Override
    public boolean performClick() {
        if (common.isLogging)
            Log.d(TAG, "performClick PuzzleSurface");

        super.performClick();
        return true;
    }

    /**
     * Perform Click is called on UP press to perform accessibility type
     * actions.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        synchronized (puzzleUpdateAndDraw.getSurfaceHolder()) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                performClick();
            }

            if (common.isWindowInFocus && common.isImageLoaded) {
                if (common.isPuzzleSolved) {
                    common.toggleUIOverlay();
                    return false;
                } else {
                    return puzzle.onTouchEvent(event);
                }
            }
            return super.onTouchEvent(event);
        }
    }

    public void createNewSizedPuzzle(int sides) {
        if (common.isLogging)
            Log.d(TAG, "createNewSizedPuzzle PuzzleSurface");

        common.isImageLoaded = false;
        puzzle = new AdjustablePuzzle(ps);
        puzzle.initPieces(sides);
        puzzle.getNewImageLoadedScaledDivided();
        common.hideButtons();
    }

    public void createPuzzle() {
        if (common.isLogging)
            Log.d(TAG, "createPuzzle PuzzleSurface");

        common.isImageLoaded = false;
        puzzle = new AdjustablePuzzle(ps);
        puzzle.initPieces(3);
        puzzle.getNewImageLoadedScaledDivided();
        common.hideButtons();
    }

    public void resumePuzzle() {
        if (common.isLogging)
            Log.d(TAG, "resumePuzzle PuzzleSurface");

        common.isImageLoaded = false;
        puzzle = new AdjustablePuzzle(ps);
        int sides = (int) common.dimensions;
        puzzle.initPieces(sides);
        puzzle.getPrevousImageLoadedScaledDivided();
        common.hideButtons();
    }

    public void pixivActivity() {
        common.hideButtons();
        Intent intent2 = new Intent(Intent.ACTION_VIEW);
        intent2.setData(Uri.parse(common.context.getString(R.string.pixiv_link)));
        common.context.startActivity(intent2);
    }

    public void wordpressActivity() {
        common.hideButtons();
        Intent intent1 = new Intent(Intent.ACTION_VIEW);
        intent1.setData(Uri.parse(common.context
                .getString(R.string.wordpress_link)));
        common.context.startActivity(intent1);
    }

    public void cleanUp() {
        if (puzzle != null)
            puzzle.recylceAll();
    }

    public String getSlotString() {
        String s = "";
        if (common.puzzleSlots != null)
            for (int i = 0; i < common.puzzleSlots.length; i++) {
                if (common.puzzleSlots[i] != null) {
                    if (i == 0) {
                        s = "" + common.puzzleSlots[i].puzzlePiece.pieceNum;
                    } else {
                        s = s + "," + common.puzzleSlots[i].puzzlePiece.pieceNum;
                    }
                }
            }
        return s;
    }

    public AlertDialog dialog;

    public void newPuzzle() {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(common.context);

        builder.setTitle("Create New Puzzle");
        builder.setMessage("Enter number of sides 2 - 7");

        final EditText inputH = new EditText(common.context);
        inputH.setInputType(InputType.TYPE_CLASS_NUMBER);
        inputH.setText(defaultPuzzleSize);
        builder.setView(inputH);

        builder.setPositiveButton("Create",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            String s = inputH.getText().toString();
                            s.replaceAll("[^0-9]", "");
                            int sides = Integer.parseInt(s);

                            if (sides > 7 || sides < 2) {
                                common.showToast("2 to 7 dimension limit");
                            } else {
                                createNewSizedPuzzle(sides);
                            }
                        } catch (NumberFormatException nfe) {
                            common.showToast("Unable to parse number entered.");
                        }
                    }
                });

        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });
        dialog = builder.show();
    }

    public void toggleMusic() {
        if (common.playMusic) {
            common.playMusic = false;
            myMediaPlayer.pause();
            common.showToast("Music Off");
        } else {
            common.playMusic = true;
            myMediaPlayer.resume();
            common.showToast("Music On");
        }
    }

    public void toggleSetSound() {
        if (common.playTapSound) {
            common.playTapSound = false;
            common.showToast("Set Effect Off");
        } else {
            common.playTapSound = true;
            common.showToast("Set Effect On");
        }
    }

    public void toggleBorder() {
        if (common.drawBorders) {
            common.drawBorders = false;
            common.showToast("Borders Off");
        } else {
            common.drawBorders = true;
            common.showToast("Borders On");
        }
    }

    public void toggleWinSound() {
        if (common.playChimeSound) {
            common.playChimeSound = false;
            common.showToast("Win Effect Off");
        } else {
            common.playChimeSound = true;
            common.showToast("Win Effect On");
        }
    }

    public void onPause() {
        if (common.isLogging)
            Log.d(TAG, "onPause PuzzleSurface");

        if (puzzleUpdateAndDraw != null) {
            puzzleUpdateAndDraw.pause();
        }
    }

    public class PuzzleUpdateAndDraw {

        public SurfaceHolder mSurfaceHolder;

        public PuzzleUpdateAndDraw(SurfaceHolder surfaceHolder,
                                   Context context) {
            mSurfaceHolder = surfaceHolder;
            common.context = context;
        }

        public void updateAndDraw() {
            if (common.isLogging)
                Log.d(TAG, "updateAndDraw PuzzleSurface");

            Canvas c = null;
            try {
                c = mSurfaceHolder.lockCanvas(null);
                if (c != null) {
                    synchronized (mSurfaceHolder) {
                        updatePhysics();
                        doDraw(c);
                    }
                }
            } finally {
                if (c != null) {
                    mSurfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }

        public SurfaceHolder getSurfaceHolder() {
            return mSurfaceHolder;
        }

        private void updatePhysics() {
            if (common.isLogging)
                Log.d(TAG, "updatePhysics PuzzleSurface");

            if (common.isPuzzleSolved) {
                common.showButtons();
            }
        }

        private void doDraw(Canvas canvas) {
            if (common.isLogging)
                Log.d(TAG, "doDraw PuzzleSurface");

            if (canvas != null) {
                canvas.drawColor(Color.BLACK);
                if (common.isImageLoaded) {
                    if (common.movingPiece) {
                        drawImageWithMovingPiece(canvas);
                    } else {
                        drawImage(canvas);
                    }
                } else {
                    // the image is still loading or in error
                    if (common.isImageError) {
                        canvas.drawColor(Color.RED);
                    } else {
                        canvas.drawColor(Color.BLUE);
                    }
                }
            }
        }

        private void drawImage(Canvas canvas) {
            if (common.isLogging)
                Log.d(TAG, "drawImage PuzzleSurface");

            for (int i = 0; i < common.numberOfPieces; i++) {
                if (!common.puzzleSlots[i].puzzlePiece.bitmap
                        .isRecycled()) {
                    // draw pieces
                    canvas.drawBitmap(
                            common.puzzleSlots[i].puzzlePiece.bitmap,
                            common.puzzleSlots[i].puzzlePiece.px,
                            common.puzzleSlots[i].puzzlePiece.py, null);
                    // draw borders
                    if (!common.isPuzzleSolved && common.drawBorders) {
                        canvas.drawRect(
                                common.puzzleSlots[i].sx,
                                common.puzzleSlots[i].sy,
                                common.puzzleSlots[i].sx
                                        + common.puzzleSlots[i].puzzlePiece.bitmap
                                        .getWidth(),
                                common.puzzleSlots[i].sy
                                        + common.puzzleSlots[i].puzzlePiece.bitmap
                                        .getHeight(),
                                borderPaintA);
                    }
                }
            }
        }

        private void drawImageWithMovingPiece(Canvas canvas) {
            for (int i = 0; i < common.numberOfPieces; i++) {
                // draw pieces
                if (!common.puzzleSlots[i].puzzlePiece.bitmap
                        .isRecycled()
                        && common.currSlotOnTouchDown != i)
                    canvas.drawBitmap(
                            common.puzzleSlots[i].puzzlePiece.bitmap,
                            common.puzzleSlots[i].puzzlePiece.px,
                            common.puzzleSlots[i].puzzlePiece.py, null);
                // draw border to pieces
                if (!common.isPuzzleSolved && common.drawBorders)
                    canvas.drawRect(
                            common.puzzleSlots[i].sx,
                            common.puzzleSlots[i].sy,
                            common.puzzleSlots[i].sx
                                    + common.puzzleSlots[i].puzzlePiece.bitmap
                                    .getWidth(),
                            common.puzzleSlots[i].sy
                                    + common.puzzleSlots[i].puzzlePiece.bitmap
                                    .getHeight(),
                            borderPaintA);
            }

            // draw moving piece and its shadow
            if (!common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                    .isRecycled()) {

                // draw moving image in original location
                canvas.drawBitmap(
                        common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap,
                        common.puzzleSlots[common.currSlotOnTouchDown].sx,
                        common.puzzleSlots[common.currSlotOnTouchDown].sy,
                        transPaint);

                // draw border around original piece location
                canvas.drawRect(
                        common.puzzleSlots[common.currSlotOnTouchDown].sx,
                        common.puzzleSlots[common.currSlotOnTouchDown].sy,
                        common.puzzleSlots[common.currSlotOnTouchDown].sx
                                + common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                                .getWidth(),
                        common.puzzleSlots[common.currSlotOnTouchDown].sy
                                + common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                                .getHeight(), borderPaintB);

                // draw moving piece
                canvas.drawBitmap(
                        common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap,
                        common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.px,
                        common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.py,
                        fullPaint);

                // draw border around moving piece
                if (common.drawBorders)
                    canvas.drawRect(
                            common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.px
                                    + (STROKE_VALUE / 2),
                            common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.py
                                    + (STROKE_VALUE / 2),
                            common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.px
                                    + common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                                    .getWidth()
                                    - (STROKE_VALUE / 2),
                            common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.py
                                    + common.puzzleSlots[common.currSlotOnTouchDown].puzzlePiece.bitmap
                                    .getHeight()
                                    - (STROKE_VALUE / 2),
                            borderPaintA);
            }
        }

        public void surfaceChanged(int width, int height) {
            // synchronized to make sure these all change atomically
            synchronized (mSurfaceHolder) {
                common.screenW = width;
                common.screenH = height;
            }
        }

        public void pause() {
            synchronized (mSurfaceHolder) {
                if (puzzle != null)
                    puzzle.pause();
            }
        }
    }
}
package reihino.data;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.util.Arrays;

import reihino.puzzleimage.CommonVariables;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Called to store the game state, currently doesn't store all objects just
 * player data, place and health.
 * 
 * @author Rick
 * 
 */

public class SaveStateDb {

	private SQLiteDatabase stateDB;

	private static final String DATABASE_NAME = "state.db";
	private static final String STATE_TABLE = "state";

	private static final String COLUMN_ID = "ID";
	private static final String COLUMN_DIFFICULTY = "DIFFICULTY";
	private static final String COLUMN_IMAGENUMBER = "IMAGENUMBER";
	private static final String COLUMN_SLOTS = "COLUMNSLOTS";

	CommonVariables cv = new CommonVariables();

	public SaveStateDb() {

	}

	public void saveState(int difficulty, int imageNumber, String slotString)
			throws IOException {
		// Add the values

		cv = CommonVariables.getInstance();

		try {
			// if in game then save state
			stateDB = cv.context.openOrCreateDatabase(DATABASE_NAME,
					SQLiteDatabase.CREATE_IF_NECESSARY
							| SQLiteDatabase.OPEN_READWRITE, null);

			stateDB.execSQL("CREATE TABLE IF NOT EXISTS " + STATE_TABLE + " ("
					+ COLUMN_ID + " INTEGER PRIMARY KEY," + COLUMN_DIFFICULTY
					+ " INTEGER," + COLUMN_IMAGENUMBER + " INTEGER,"
					+ COLUMN_SLOTS + " VARCHAR" + ")");

			ContentValues values = new ContentValues();

			boolean saveOk = true;
			// check for slots to be set
			String[] slotArr = slotString.split(",");
			int[] intSlotArr = new int[slotArr.length];
			for (int i = 0; i < intSlotArr.length; i++) {
				intSlotArr[i] = Integer.parseInt(slotArr[i]);
			}

			// sort array and check for each number in it
			Arrays.sort(intSlotArr);
			for (int i = 0; i < intSlotArr.length; i++) {
				if (intSlotArr[i] != i)
					saveOk = false;
			}

			if (saveOk) {
				values.put(COLUMN_DIFFICULTY, difficulty);
				values.put(COLUMN_IMAGENUMBER, imageNumber);
				values.put(COLUMN_SLOTS, slotString);
			}

			stateDB.insert(STATE_TABLE, null, values);

		} catch (Exception e) {
			// if exception occurred in saving delete faulty db
			delete();
		} finally {
			if (stateDB.isOpen()) {
				stateDB.close();
			}
		}
	}

	public boolean getState() throws StreamCorruptedException, IOException,
			ClassNotFoundException {
		cv = CommonVariables.getInstance();

		try {
			stateDB = cv.context.openOrCreateDatabase(DATABASE_NAME,
					SQLiteDatabase.CREATE_IF_NECESSARY
							| SQLiteDatabase.OPEN_READWRITE, null);

			stateDB.execSQL("CREATE TABLE IF NOT EXISTS " + STATE_TABLE + " ("
					+ COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_DIFFICULTY
					+ " INTEGER," + COLUMN_IMAGENUMBER + " INTEGER,"
					+ COLUMN_SLOTS + " VARCHAR" + ")");

			Cursor c = stateDB.query(STATE_TABLE, null, null, null, null, null,
					null);

			if (c.moveToFirst()) {
				// check for valid slots to be set
				String[] slotArr = c.getString(3).split(",");
				int[] intSlotArr = new int[slotArr.length];
				for (int i = 0; i < intSlotArr.length; i++) {
					intSlotArr[i] = Integer.parseInt(slotArr[i]);
				}

				// sort array and check for each number in it
				Arrays.sort(intSlotArr);
				cv.resumePreviousPuzzle = true;
				for (int i = 0; i < intSlotArr.length; i++) {
					if (intSlotArr[i] != i)
						cv.resumePreviousPuzzle = false;
				}
				
				if(cv.resumePreviousPuzzle){
					// game will restart with old objects
					cv.difficulty = c.getInt(1);
					cv.currentImage = c.getInt(2);
					cv.setSlots(c.getString(3));
				}

				c.close();
				delete();
				return true;
			}
		} catch (Exception e) {
			// if error out delete bad db... no outer join
			delete();
		} finally {
			if (stateDB.isOpen()) {
				stateDB.close();
			}
		}
		return false;
	}

	/**
	 * There should only be one save state. If the player closes the app in the
	 * menu or game over screen there shouldn't be the previous record to
	 * re-reload.
	 */
	public void delete() {
		// delete previous save record now that it is reloaded
		stateDB.execSQL("DROP TABLE IF EXISTS " + STATE_TABLE);
	}
}
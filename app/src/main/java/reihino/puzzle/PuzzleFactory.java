package reihino.puzzle;

import reihino.puzzleimage.EasyPuzzleImpl;
import reihino.puzzleimage.HardPuzzleImpl;
import reihino.puzzleimage.VeryHardPuzzleImpl;

public class PuzzleFactory {

	public Puzzle puzzle;

	public Puzzle getPuzzle(int diff) {
		switch (diff) {
		case 0:
			puzzle = new EasyPuzzleImpl();
			return puzzle;
		case 1:
			puzzle = new HardPuzzleImpl();
			return puzzle;
		case 2: 
			puzzle = new VeryHardPuzzleImpl();
			return puzzle;
		default:
			return null;
		}
	}
}

package reihino.puzzleimage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import reihino.puzzle.Puzzle;
import reihino.puzzle.PuzzleFactory;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.TransitionDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class PuzzleSurface extends SurfaceView implements
		SurfaceHolder.Callback, MediaPlayer.OnPreparedListener,
		MediaPlayer.OnErrorListener {

	public static final int EASY = 0;
	public static final int HARD = 1;
	public static final int VERY_HARD = 2;

	public static final int STATE_RESUMING = 0;
	public static final int STATE_LOSE = 1;
	public static final int STATE_PAUSE = 2;
	public static final int STATE_READY = 3;
	public static final int STATE_RUNNING = 4;
	public static final int STATE_WIN = 5;

	Thread loadingThread;

	public Intent intent;
	public boolean mExternalStorageAvailable;
	boolean mExternalStorageWriteable;
	boolean loaded;
	public Toast toast;
	public SoundPool sp;

	public AudioManager audioManager;
	public float actualVolume, maxVolume;
	public TransitionDrawable buttonTrans;

	public boolean playMusic;
	public MediaPlayer mp;
	public int soundStopped;

	public Paint borderPaint;

	PuzzleFactory pf = new PuzzleFactory();
	Puzzle puzzle;

	boolean difficultyChanged, playWinSound = true;
	Paint paint = new Paint();

	CommonVariables cv;
	Handler myHandler;

	class IncomingHandlerCallback implements Handler.Callback {
		@Override
		public boolean handleMessage(Message m) {
			// handle message code
			cv.mStatusText.setVisibility(m.getData().getInt("viz"));
			cv.mStatusText.setText(m.getData().getString("text"));
			return true;
		}
	}

	@SuppressWarnings("deprecation")
	public PuzzleSurface(Context context, AttributeSet attrs) {
		super(context, attrs);

		cv = CommonVariables.getInstance();
		cv.context = context;

		// register our interest in hearing about changes to our surface
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);

		paint.setColor(Color.WHITE);
		paint.setTextSize(22);

		borderPaint = new Paint(Color.BLACK);
		borderPaint.setStyle(Paint.Style.STROKE);
		borderPaint.setStrokeWidth(10);

		myHandler = new Handler(new IncomingHandlerCallback());

		// create thread only; it's started in surfaceCreated()
		cv.thread = new PuzzleThread(holder, context, myHandler);

		audioManager = (AudioManager) cv.context
				.getSystemService(Context.AUDIO_SERVICE);
		actualVolume = (float) audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		maxVolume = (float) audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		cv.volume = actualVolume / maxVolume;

		sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 100);
		sp.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			public void onLoadComplete(SoundPool soundPool, int sampleId,
					int status) {
				loaded = true;
				playWinSound();
			}
		});

		cv.sp = sp;

		cv.saveSound = sp.load(cv.context, R.raw.imagesaved, 1);
		cv.tapSound = sp.load(cv.context, R.raw.tap, 1);

		cv.res = context.getResources();
		cv.rand = new Random();
	}

	public PuzzleThread getThread() {
		return cv.thread;
	}

	public void actaullySavePhoto() {

		buttonTrans.startTransition(500);

		// save current image to devices images folder
		String state = Environment.getExternalStorageState();
		// check if writing is an option
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but
			// all we need
			// to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}

		if (mExternalStorageAvailable && mExternalStorageWriteable) {
			// then write picture to phone
			File path = Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

			String name = Data.ARTIST + cv.currentImage + ".jpeg";

			File file = new File(path, name);
			InputStream is = null;

			// check for file in directory
			if (file.exists()) {
				showToast(cv.context, "Photo Exists Already!");
				if (loaded)
					playWinSound();
			} else {
				try {
					boolean b1 = path.mkdirs();
					boolean b2 = path.exists();
					// Make sure the Pictures directory exists.
					if (b1 || b2) {

						// get file into input stream
						is = getResources().openRawResource(
								Data.pics[cv.currentImage]);

						OutputStream os = new FileOutputStream(file);
						byte[] data = new byte[is.available()];
						is.read(data);
						os.write(data);
						is.close();
						os.close();

						showToast(cv.context, "Photo saved");

						// Tell the media scanner about the new file so that it
						// is
						// immediately available to the user.
						MediaScannerConnection
								.scanFile(
										cv.context,
										new String[] { file.toString() },
										null,
										new MediaScannerConnection.OnScanCompletedListener() {
											@Override
											public void onScanCompleted(
													String path, Uri uri) {
												// Log.i("ExternalStorage",
												// "Scanned " + path + ":");
												// Log.i("ExternalStorage",
												// "-> uri=" + uri);
											}
										});
					} else {
						showToast(cv.context,
								"Could not make/access directory.");
					}
				} catch (IOException e) {
					showToast(cv.context, "Error making/accessing directory.");
				}
			}
		} else {
			showToast(cv.context, "Directory not available/writable.");
		}
	}

	public void nextImage() {
		if (cv.mSaveButton.isShown())
			cv.mSaveButton.setVisibility(INVISIBLE);

		AlertDialog.Builder builder = new AlertDialog.Builder(cv.context);

		builder.setTitle("Radical\u2605Appwards\nSolve Time = "
				+ puzzle.getSolveTime() + " secs.");
		builder.setMessage("Do you want to Save this image?");
		builder.setPositiveButton("Yup", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (loaded)
					playWinSound();
				actaullySavePhoto();
				hideButtons();
				cv.thread.setState(PuzzleSurface.STATE_PAUSE);
				cv.imageReady = false;
				puzzle.getNewImageLoadedScaledDivided(loadingThread);
				dialog.dismiss();
			}
		});

		builder.setNegativeButton("Nope",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (loaded)
							playWinSound();
						hideButtons();
						cv.thread.setState(PuzzleSurface.STATE_PAUSE);
						cv.imageReady = false;
						puzzle.getNewImageLoadedScaledDivided(loadingThread);
						dialog.dismiss();
					}
				});

		builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (!cv.mSaveButton.isShown())
					cv.mSaveButton.setVisibility(VISIBLE);
				if (cv.mStatusText.isShown())
					cv.mStatusText.setVisibility(INVISIBLE);
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}

	public void showButtons() {
		if (cv.mSaveButton.getVisibility() == View.INVISIBLE)
			cv.mSaveButton.setVisibility(VISIBLE);
		if (cv.devArtButton.getVisibility() == View.INVISIBLE)
			cv.devArtButton.setVisibility(VISIBLE);
		if (cv.pixivButton.getVisibility() == View.INVISIBLE)
			cv.pixivButton.setVisibility(VISIBLE);
	}

	public void hideButtons() {
		if (cv.mSaveButton.getVisibility() == View.VISIBLE)
			cv.mSaveButton.setVisibility(INVISIBLE);
		if (cv.devArtButton.getVisibility() == View.VISIBLE)
			cv.devArtButton.setVisibility(INVISIBLE);
		if (cv.pixivButton.getVisibility() == View.VISIBLE)
			cv.pixivButton.setVisibility(INVISIBLE);
	}

	public void showToast(Context cont, String message) {
		// create if not, or set text to it
		if (toast == null) {
			toast = Toast.makeText(cont, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
		}
		if (!toast.getView().isShown()) {
			toast.setText(message);
			toast.show();
		}
	}

	public void setButton(Button button) {
		cv.mSaveButton = button;
		cv.mSaveButton.setBackgroundResource(R.drawable.custom_button);
		buttonTrans = (TransitionDrawable) cv.mSaveButton.getBackground();
		buttonTrans.startTransition(500);
	}

	public void setDevArtButton(ImageButton daButton) {
		cv.devArtButton = daButton;
	}

	public void setPixivButton(ImageButton pixButton) {
		cv.pixivButton = pixButton;
	}

	public void setTextView(TextView textView) {
		cv.mStatusText = textView;
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		if (!hasWindowFocus) {
			cv.thread.pause();
		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		cv.thread.setSurfaceSize(width, height);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// look for a thread that is either dead or new
		if (cv.thread == null
				|| cv.thread.getState() == Thread.State.TERMINATED) {
			cv.thread = new PuzzleThread(holder, cv.context, myHandler);
		}

		// only start if its a new thread, an older thread may still be started
		if (cv.thread.getState() == Thread.State.NEW)
			cv.thread.start();

		cv.thread.setRunning(true);

	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		cv.thread.setRunning(false);
		while (retry) {
			try {
				cv.thread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (getThread().getSurfaceHolder()) {
			if (cv.thread.mMode == STATE_READY) {
				hideButtons();
				cv.thread.doStart();
				startTimer();
				return false;
			} else if (cv.thread.mMode == STATE_PAUSE
					|| cv.thread.mMode == STATE_RESUMING) {
				if (cv.mStatusText.isShown()) {
					cv.mStatusText.setVisibility(View.INVISIBLE);
					hideButtons();
				}
				cv.thread.doStart();
				return false;
			} else if (cv.thread.mMode == STATE_RUNNING) {
				if (cv.imageReady) {
					return puzzle.onTouchEvent(event);
				}
			} else if (cv.thread.mMode == STATE_WIN) {
				// win screen is before the next image button is pressed
				if (cv.mSaveButton.getVisibility() == View.VISIBLE)
					cv.mSaveButton.setVisibility(INVISIBLE);
				else
					cv.mSaveButton.setVisibility(VISIBLE);

				if (cv.devArtButton.getVisibility() == View.VISIBLE)
					cv.devArtButton.setVisibility(INVISIBLE);
				else
					cv.devArtButton.setVisibility(VISIBLE);

				if (cv.pixivButton.getVisibility() == View.VISIBLE)
					cv.pixivButton.setVisibility(INVISIBLE);
				else
					cv.pixivButton.setVisibility(VISIBLE);

				return false;

			}
			return true;
		}
	}

	public void playWinSound() {
		if (playWinSound)
			sp.play(cv.saveSound, cv.volume, cv.volume, 1, 0, 1f);
	}

	public void setCurrPieceOnTouch(int newPiece) {
		cv.currPieceOnTouch = newPiece;
	}

	public void startMp(MediaPlayer m) {
		playMusic = true;
		mp = m;
		startMp();
	}

	public void recreatePuzzle() {
		hideButtons();
		cv.thread.setState(STATE_PAUSE);
		puzzle = pf.getPuzzle(cv.difficulty);
		cv.imageReady = false;
		puzzle.getNewImageLoadedScaledDivided(loadingThread);
	}

	public void pauseMp() {
		mp.pause();
	}

	public void startMp() {
		try {
			Uri path = Uri.parse(Data.PATH + R.raw.urb);
			mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mp.reset();
			mp.setDataSource(cv.context, path);
			mp.setLooping(true);
			mp.setVolume(1, 1);
			mp.setOnPreparedListener(this);
			mp.prepareAsync();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
		// Log.d("radio", "error " + i + " " + i2);
		return false; // To change body of implemented methods use File |
						// Settings | File Templates.
	}

	@Override
	public void onPrepared(MediaPlayer player) {
		if (playMusic) {
			player.start();
			if (soundStopped > 0) {
				mp.seekTo(soundStopped);
			}
		}
	}

	public int getSoundStopped() {
		return soundStopped;
	}

	public void setSoundStopped(int currentPosition) {
		soundStopped = currentPosition;
	}

	public void solve() {
		for (int i = 0; i < cv.numberOfPieces; i++) {
			int oldslot = 8 - i;
			int newslot = cv.slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = cv.puzzleSlots[oldslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece = cv.puzzleSlots[newslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece.px = cv.puzzleSlots[oldslot].sx;
				cv.puzzleSlots[oldslot].puzzlePiece.py = cv.puzzleSlots[oldslot].sy;
				cv.puzzleSlots[newslot].puzzlePiece = temp;
				cv.puzzleSlots[newslot].puzzlePiece.px = cv.puzzleSlots[newslot].sx;
				cv.puzzleSlots[newslot].puzzlePiece.py = cv.puzzleSlots[newslot].sy;
				temp = null;
			}
		}
		cv.solved = true;
	}

	public void devartActivity() {
		cv.thread.pause();
		Intent intent2 = new Intent(Intent.ACTION_VIEW);
		intent2.setData(Uri.parse(Data.DEVART_LINK));
		cv.context.startActivity(intent2);
	}

	public void pixivActivity() {
		cv.thread.pause();
		Intent intent2 = new Intent(Intent.ACTION_VIEW);
		intent2.setData(Uri.parse(Data.PIXIV_LINK));
		cv.context.startActivity(intent2);
	}

	public void cleanUp() {
		puzzle.recylceAll();
		if (loadingThread != null && loadingThread.isAlive())
			loadingThread.interrupt();
	}

	public void startTimer() {
		// start timer for new puzzle in puzzle
		if (puzzle != null)
			puzzle.initTimer();
	}

	public void pause() {
		puzzle.stopTimer();
		cv.thread.pause();
		cv.thread.setState(STATE_PAUSE);
		hideButtons();
	}

	public void resume(MediaPlayer mp) {
		startMp(mp);
		startTimer();
	}

	public String getSlotString() {
		String s = "";
		for (int i = 0; i < cv.puzzleSlots.length; i++) {
			if (i == 0) {
				s = "" + cv.puzzleSlots[i].puzzlePiece.pieceNum;
			} else {
				s = s + "," + cv.puzzleSlots[i].puzzlePiece.pieceNum;
			}
		}
		return s;
	}

	public void jumblePictureFromOldPuzzle() {
		for (int i = 0; i < cv.numberOfPieces; i++) {
			int oldslot = i;
			int newslot = cv.slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = cv.puzzleSlots[oldslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece = cv.puzzleSlots[newslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece.px = cv.puzzleSlots[oldslot].sx;
				cv.puzzleSlots[oldslot].puzzlePiece.py = cv.puzzleSlots[oldslot].sy;
				cv.puzzleSlots[newslot].puzzlePiece = temp;
				cv.puzzleSlots[newslot].puzzlePiece.px = cv.puzzleSlots[newslot].sx;
				cv.puzzleSlots[newslot].puzzlePiece.py = cv.puzzleSlots[newslot].sy;
				temp = null;
			}
		}
	}

	public class PuzzleThread extends Thread {

		public boolean mRun = false;
		public SurfaceHolder mSurfaceHolder;
		public int mMode;
		public Handler mHandler;

		public PuzzleThread(SurfaceHolder surfaceHolder, Context context,
				Handler handler) {
			// get handles to some important objects
			mSurfaceHolder = surfaceHolder;
			mHandler = handler;
			cv.context = context;
		}

		@Override
		public void run() {
			while (mRun) {
				Canvas c = null;
				try {
					c = mSurfaceHolder.lockCanvas(null);
					synchronized (mSurfaceHolder) {
						if (mMode == STATE_RUNNING)
							updatePhysics();
						doDraw(c);
					}
				} finally {
					if (c != null) {
						mSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}

		public void setRunning(boolean b) {
			mRun = b;
		}

		//
		public SurfaceHolder getSurfaceHolder() {
			return mSurfaceHolder;
		}

		public void updatePhysics() {
			if (difficultyChanged) {
				difficultyChanged = false;
				recreatePuzzle();
			}
		}

		public void doDraw(Canvas canvas) {
			if (canvas != null) {
				if (puzzle == null) {
					if (cv.resumePreviousPuzzle) {
						puzzle = pf.getPuzzle(cv.difficulty);
						cv.imageReady = false;
						puzzle.getPrevousImageLoadedScaledDivided(loadingThread);
						puzzle.initTimer();
					} else {
						// start with an easy puzzle
						puzzle = pf.getPuzzle(EASY);
						cv.imageReady = false;
						puzzle.getNewImageLoadedScaledDivided(loadingThread);
						puzzle.initTimer();
					}
				} else if (cv.imageReady) {

					canvas.drawColor(Color.BLACK);

					for (int i = 0; i < cv.numberOfPieces; i++) {
						if (!cv.puzzleSlots[i].puzzlePiece.bitmap.isRecycled())
							canvas.drawBitmap(
									cv.puzzleSlots[i].puzzlePiece.bitmap,
									cv.puzzleSlots[i].puzzlePiece.px,
									cv.puzzleSlots[i].puzzlePiece.py, null);
					}

					if (cv.movingPiece) {
						canvas.drawRect(
								cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px,
								cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py,
								cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px
										+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
												.getWidth(),
								cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py
										+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
												.getHeight(), borderPaint);
						if (!cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
								.isRecycled())
							canvas.drawBitmap(
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap,
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px,
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py,
									null);
					}
				} else {
					// the image is loading
					if (cv.errorLoading)
						canvas.drawColor(Color.GREEN);
					else
						canvas.drawColor(Color.BLACK);
				}
			}
		}

		public void toggleSetSound() {
			if (cv.playSetSound) {
				cv.playSetSound = false;
				showToast(cv.context, "Set Effect Off");
			} else {
				cv.playSetSound = true;
				showToast(cv.context, "Set Effect On");
			}
		}

		public void toggleWinSound() {
			if (playWinSound) {
				playWinSound = false;
				showToast(cv.context, "Win Effect Off");
			} else {
				playWinSound = true;
				showToast(cv.context, "Win Effect On");
			}
		}

		public void toggleMusic() {
			if (playMusic) {
				playMusic = false;
				pauseMp();
				showToast(cv.context, "Music Off");
			} else {
				playMusic = true;
				startMp();
				showToast(cv.context, "Music On");
			}
		}

		public synchronized void restoreState(Bundle savedInstanceState) {
			synchronized (mSurfaceHolder) {
				// cv.difficulty = savedInstanceState.getInt("difficulty");
				// cv.currentImage = savedInstanceState.getInt("currentimage");
				// cv.slots = savedInstanceState.getIntArray("slots");
				// cv.resumePreviousPuzzle = true;
				// setState(STATE_PAUSE);
			}
		}

		public void setState(int mode) {
			synchronized (mSurfaceHolder) {
				setState(mode, null);
			}
		}

		public void setState(int mode, CharSequence message) {
			synchronized (mSurfaceHolder) {
				mMode = mode;

				if (mMode == STATE_RUNNING) {
					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("text", "");
					b.putInt("viz", View.INVISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
				} else {
					Resources res = cv.context.getResources();
					CharSequence str = "";
					if (mMode == STATE_READY)
						str = res.getText(R.string.mode_ready);
					else if (mMode == STATE_PAUSE)
						str = res.getText(R.string.mode_pause);
					else if (mMode == STATE_LOSE)
						str = res.getText(R.string.mode_lose);
					else if (mMode == STATE_WIN)
						str = "";

					if (message != null) {
						str = message + "\n" + str;
					}

					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("text", str.toString());
					b.putInt("viz", View.VISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
				}
			}
		}

		public void setSurfaceSize(int width, int height) {
			// synchronized to make sure these all change atomically
			synchronized (mSurfaceHolder) {
				cv.screenW = width;
				cv.screenH = height;
			}
		}

		public void doStart() {
			synchronized (mSurfaceHolder) {
				// First set the game for Medium difficulty

				// Adjust difficulty for EASY/HARD
				if (cv.difficulty == EASY) {

				} else if (cv.difficulty == HARD) {

				} else if (cv.difficulty == VERY_HARD) {

				}

				setState(STATE_RUNNING);
			}
		}

		public void pause() {
			synchronized (mSurfaceHolder) {
				if (mMode == STATE_RUNNING)
					setState(STATE_PAUSE);
			}
		}

		public void setDifficulty(int difficulty) {
			synchronized (mSurfaceHolder) {
				if (!cv.imageReady)
					showToast(cv.context,
							"Please wait for current image to load first.");
				else {
					if (cv.thread.mMode != STATE_WIN) {
						if (difficulty != cv.difficulty) {
							cv.difficulty = difficulty;
							difficultyChanged = true;
						}
					} else {
						showToast(cv.context,
								"Press next to save current image first.");
					}
				}
			}
		}

		public Bundle saveState(Bundle map) {
			synchronized (mSurfaceHolder) {
				if (map != null) {
					// map.putInt("difficulty", cv.difficulty);
					// map.putInt("currentimage", puzzle.getCurrentImage());
					// setSlotArray();
					// map.putIntArray("slots", slots);
				}
			}
			return map;
		}
	}
}
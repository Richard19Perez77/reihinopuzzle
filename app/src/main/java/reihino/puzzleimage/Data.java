
package reihino.puzzleimage;


public class Data {
	
	public static int[] pics = { R.drawable.image0, R.drawable.image1,
			R.drawable.image2, R.drawable.image3, R.drawable.image4,
			R.drawable.image5, R.drawable.image6, R.drawable.image7,
			R.drawable.image8, R.drawable.image9, R.drawable.image10,
			R.drawable.image11, R.drawable.image12, R.drawable.image13,
			R.drawable.image14, R.drawable.image15 };
	
	public static String PIXIV_LINK = "http://www.pixiv.net/member.php?id=6403107";
	public static String DEVART_LINK = "http://reihino5.deviantart.com/";
	public static String ARTIST = "reihino";
	public static String PATH = "android.resource://reihino.puzzleimage/";
	
}

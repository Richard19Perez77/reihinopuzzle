package reihino.puzzleimage;

public class PuzzleSlot {
	public int sx, sy, sx2, sy2;
	public PuzzlePiece puzzlePiece;
	public int slotNum;
	
	public PuzzleSlot(){
		puzzlePiece = new PuzzlePiece();
	}

}

package reihino.puzzleimage;

import java.util.ArrayList;
import java.util.Random;

import reihino.puzzleimage.PuzzleSurface.PuzzleThread;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.SoundPool;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class CommonVariables {

	// /Setup up for commonly used variables that can be accessed when the class
	// is called
	private volatile static CommonVariables instance;

	public Context context;
	public int difficulty;
	public int currentImage;
	public boolean resumePreviousPuzzle;
	public boolean imageReady;
	// the value is the piece to go into it
	public int[] slotOrder;
	public ArrayList<Integer> imagesShown = new ArrayList<Integer>();
	public Random rand = new Random();
	public Resources res;
	public int screenH, screenW;
	public Bitmap image;
	public PuzzlePiece[] puzzlePieces;
	public PuzzleSlot[] puzzleSlots;
	public boolean errorLoading;
	public boolean solved;
	public int numberOfPieces;
	public boolean imageSplit;
	public boolean movingPiece;
	public int currPieceOnTouch;
	public int currSlotOnTouchUp;
	public int inPlace;
	public TextView mStatusText;
	public Button mSaveButton;
	public PuzzleThread thread;
	public ImageButton devArtButton, pixivButton;
	public boolean playSetSound = true;
	public SoundPool sp;
	public int saveSound;
	public int tapSound;
	public float volume;

	public static CommonVariables getInstance() {
		if (instance == null)
			synchronized (CommonVariables.class) {
				if (instance == null)
					instance = new CommonVariables();
			}
		return instance;
	}

	public void playSetSound() {
		if (playSetSound)
			sp.play(tapSound, volume, volume, 1, 0, 1f);
	}

	public void setSlots(String string) {
		switch (difficulty) {
		case 0:
			slotOrder = new int[9];
			break;
		case 1:
			slotOrder = new int[16];
			break;
		case 2:
			slotOrder = new int[25];
			break;
		}
		String[] stringSlots = string.split(",");
		for (int i = 0; i < stringSlots.length; i++) {
			slotOrder[i] = Integer.parseInt(stringSlots[i]);
		}
	}

	public void jumblePicture() {
		// reorders the images as jumbled
		for (int i = 0; i < numberOfPieces; i++) {
			int oldslot = i;
			int newslot = slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = puzzleSlots[oldslot].puzzlePiece;
				puzzleSlots[oldslot].puzzlePiece = puzzleSlots[newslot].puzzlePiece;
				puzzleSlots[oldslot].puzzlePiece.px = puzzleSlots[oldslot].sx;
				puzzleSlots[oldslot].puzzlePiece.py = puzzleSlots[oldslot].sy;
				puzzleSlots[newslot].puzzlePiece = temp;
				puzzleSlots[newslot].puzzlePiece.px = puzzleSlots[newslot].sx;
				puzzleSlots[newslot].puzzlePiece.py = puzzleSlots[newslot].sy;
				temp = null;
			}
		}
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}


	// switch a piece in the puzzle
	public void sendPieceToNewSlot(int a, int z) {
		PuzzlePiece temp = new PuzzlePiece();
		temp = puzzleSlots[currPieceOnTouch].puzzlePiece;
		puzzleSlots[a].puzzlePiece = puzzleSlots[z].puzzlePiece;
		puzzleSlots[a].puzzlePiece.px = puzzleSlots[a].sx;
		puzzleSlots[a].puzzlePiece.py = puzzleSlots[a].sy;
		puzzleSlots[z].puzzlePiece = temp;
		puzzleSlots[z].puzzlePiece.px = puzzleSlots[z].sx;
		puzzleSlots[z].puzzlePiece.py = puzzleSlots[z].sy;
		temp = null;
	}
}
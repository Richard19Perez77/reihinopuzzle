package reihino.puzzleimage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import reihino.puzzle.Puzzle;
import android.graphics.Bitmap;
import android.view.MotionEvent;
import android.view.View;

public class VeryHardPuzzleImpl implements Puzzle {

	public int PIECES = 25;
	int bitmapWd5;
	int bitmapHd5;

	public int piecesComplete;
	int jumbledNumber;
	int index;
	boolean isRandom, newImageComplete;
	public Date startPuzzle = new Date();
	public Date stopPuzzle = new Date();
	long currPuzzleTime = 0;
	CommonVariables cv;

	public VeryHardPuzzleImpl() {
		cv = CommonVariables.getInstance();
	}

	public void getNewImageLoadedScaledDivided(Thread thread) {
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {

				while (!newImageComplete) {
					// fill with all valid numbers
					if (cv.imagesShown.isEmpty())
						for (int i = 0; i < Data.pics.length; i++)
							cv.imagesShown.add(i);

					// get new index value from remaining images
					index = cv.rand.nextInt(cv.imagesShown.size());
					// get the value at that index for new image
					cv.currentImage = cv.imagesShown.get(index);
					// remove from list to prevent duplicates
					cv.imagesShown.remove(index);

					cv.image = cv.decodeSampledBitmapFromResource(cv.res,
							Data.pics[cv.currentImage], cv.screenW, cv.screenH);
					cv.image = Bitmap.createScaledBitmap(cv.image, cv.screenW,
							cv.screenH, true);

					newImageComplete = divideBitmap();

					if (newImageComplete) {
						resetTimer();
						cv.errorLoading = false;
						cv.solved = false;
						cv.imageReady = true;
					} else {
						cv.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	public boolean divideBitmap() {
		cv.numberOfPieces = PIECES;

		cv.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < cv.numberOfPieces; i++)
			cv.puzzlePieces[i] = new PuzzlePiece();

		cv.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < cv.numberOfPieces; i++)
			cv.puzzleSlots[i] = new PuzzleSlot();

		cv.slotOrder = new int[PIECES];

		// set natural order
		for (int i = 0; i < PIECES; i++)
			cv.slotOrder[i] = i;

		isRandom = false;
		while (!isRandom) {

			List<Integer> list = new ArrayList<Integer>();
			for (int i : cv.slotOrder) {
				list.add(i);
			}
			Collections.shuffle(list);
			for (int i = 0; i < list.size(); i++) {
				cv.slotOrder[i] = list.get(i);
			}

			isRandom = true;
			if (cv.slotOrder[0] == 0 && cv.slotOrder[1] == 1
					&& cv.slotOrder[2] == 2 && cv.slotOrder[3] == 3
					&& cv.slotOrder[4] == 4 && cv.slotOrder[5] == 5
					&& cv.slotOrder[6] == 6 && cv.slotOrder[7] == 7
					&& cv.slotOrder[8] == 8) {
				isRandom = false;
			}
		}

		// re do if the image didn't split correctly
		cv.imageSplit = false;
		piecesComplete = 0;

		while (!cv.imageSplit) {
			int w = cv.image.getWidth();
			int h = cv.image.getHeight();
			bitmapWd5 = w / 5;
			bitmapHd5 = h / 5;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 5) {
					y = 0;
				} else if (i < 10) {
					y = bitmapHd5;
				} else if (i < 15) {
					y = bitmapHd5 * 2;
				} else if (i < 20) {
					y = bitmapHd5 * 3;
				} else {
					y = bitmapHd5 * 4;
				}

				x = (i % 5) * bitmapWd5;

				if (cv.puzzlePieces[i].bitmap != null)
					cv.puzzlePieces[i].bitmap.recycle();

				cv.puzzlePieces[i].bitmap = null;
				cv.puzzlePieces[i].bitmap = Bitmap.createBitmap(cv.image, x, y,
						bitmapWd5, bitmapHd5);

				cv.puzzlePieces[i].px = x;
				cv.puzzlePieces[i].px2 = x + bitmapWd5;

				cv.puzzlePieces[i].py = y;
				cv.puzzlePieces[i].py2 = y + bitmapHd5;

				cv.puzzleSlots[i].sx = x;
				cv.puzzleSlots[i].sx2 = x + bitmapWd5;

				cv.puzzleSlots[i].sy = y;
				cv.puzzleSlots[i].sy2 = y + bitmapHd5;

				cv.puzzleSlots[i].puzzlePiece = cv.puzzlePieces[i];

				cv.puzzleSlots[i].slotNum = cv.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			cv.imageSplit = true;
			cv.image.recycle();
			cv.image = null;
		}

		cv.jumblePicture();

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean divideBitmapFromPreviousPuzzle() {
		cv.numberOfPieces = PIECES;

		cv.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < cv.numberOfPieces; i++)
			cv.puzzlePieces[i] = new PuzzlePiece();

		cv.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < cv.numberOfPieces; i++)
			cv.puzzleSlots[i] = new PuzzleSlot();

		// re do if the image didn't split correctly
		cv.imageSplit = false;
		piecesComplete = 0;

		while (!cv.imageSplit) {
			int w = cv.image.getWidth();
			int h = cv.image.getHeight();
			bitmapWd5 = w / 5;
			bitmapHd5 = h / 5;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 5) {
					y = 0;
				} else if (i < 10) {
					y = bitmapHd5;
				} else if (i < 15) {
					y = bitmapHd5 * 2;
				} else if (i < 20) {
					y = bitmapHd5 * 3;
				} else {
					y = bitmapHd5 * 4;
				}

				x = (i % 5) * bitmapWd5;

				if (cv.puzzlePieces[i].bitmap != null)
					cv.puzzlePieces[i].bitmap.recycle();

				cv.puzzlePieces[i].bitmap = null;
				cv.puzzlePieces[i].bitmap = Bitmap.createBitmap(cv.image, x, y,
						bitmapWd5, bitmapHd5);

				cv.puzzlePieces[i].px = x;
				cv.puzzlePieces[i].px2 = x + bitmapWd5;

				cv.puzzlePieces[i].py = y;
				cv.puzzlePieces[i].py2 = y + bitmapHd5;

				cv.puzzleSlots[i].sx = x;
				cv.puzzleSlots[i].sx2 = x + bitmapWd5;

				cv.puzzleSlots[i].sy = y;
				cv.puzzleSlots[i].sy2 = y + bitmapHd5;

				cv.puzzleSlots[i].puzzlePiece = cv.puzzlePieces[i];
				cv.puzzleSlots[i].slotNum = cv.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			cv.imageSplit = true;
			cv.image.recycle();
			cv.image = null;
		}
		
		// use saved slot list to sort
		for (int fromSlot = 0; fromSlot < cv.slotOrder.length; fromSlot++) {
			int newslot = cv.slotOrder[fromSlot];
			PuzzlePiece temp = new PuzzlePiece();
			temp = cv.puzzleSlots[fromSlot].puzzlePiece;
			cv.puzzleSlots[fromSlot].puzzlePiece = cv.puzzleSlots[newslot].puzzlePiece;
			cv.puzzleSlots[fromSlot].puzzlePiece.px = cv.puzzleSlots[fromSlot].sx;
			cv.puzzleSlots[fromSlot].puzzlePiece.py = cv.puzzleSlots[fromSlot].sy;
			cv.puzzleSlots[fromSlot].puzzlePiece.px2 = cv.puzzleSlots[fromSlot].sx2;
			cv.puzzleSlots[fromSlot].puzzlePiece.py2 = cv.puzzleSlots[fromSlot].sy2;
			cv.puzzleSlots[newslot].puzzlePiece = temp;
			cv.puzzleSlots[newslot].puzzlePiece.px = cv.puzzleSlots[newslot].sx;
			cv.puzzleSlots[newslot].puzzlePiece.py = cv.puzzleSlots[newslot].sy;
			cv.puzzleSlots[newslot].puzzlePiece.px2 = cv.puzzleSlots[newslot].sx2;
			cv.puzzleSlots[newslot].puzzlePiece.py2 = cv.puzzleSlots[newslot].sy2;
		}

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int newx = (int) event.getX();
		int newy = (int) event.getY();

		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			cv.movingPiece = false;
			if (newx < bitmapWd5) {
				if (newy < bitmapHd5) {
					cv.currPieceOnTouch = 0;
				} else if (newy < bitmapHd5 * 2) {
					cv.currPieceOnTouch = 5;
				} else if (newy < bitmapHd5 * 3) {
					cv.currPieceOnTouch = 10;
				} else if (newy < bitmapHd5 * 4) {
					cv.currPieceOnTouch = 15;
				} else {
					cv.currPieceOnTouch = 20;
				}
			} else if (newx < bitmapWd5 * 2) {
				if (newy < bitmapHd5) {
					cv.currPieceOnTouch = 1;
				} else if (newy < bitmapHd5 * 2) {
					cv.currPieceOnTouch = 6;
				} else if (newy < bitmapHd5 * 3) {
					cv.currPieceOnTouch = 11;
				} else if (newy < bitmapHd5 * 4) {
					cv.currPieceOnTouch = 16;
				} else {
					cv.currPieceOnTouch = 21;
				}
			} else if (newx < bitmapWd5 * 3) {
				if (newy < bitmapHd5) {
					cv.currPieceOnTouch = 2;
				} else if (newy < bitmapHd5 * 2) {
					cv.currPieceOnTouch = 7;
				} else if (newy < bitmapHd5 * 3) {
					cv.currPieceOnTouch = 12;
				} else if (newy < bitmapHd5 * 4) {
					cv.currPieceOnTouch = 17;
				} else {
					cv.currPieceOnTouch = 22;
				}
			} else if (newx < bitmapWd5 * 4) {
				if (newy < bitmapHd5) {
					cv.currPieceOnTouch = 3;
				} else if (newy < bitmapHd5 * 2) {
					cv.currPieceOnTouch = 8;
				} else if (newy < bitmapHd5 * 3) {
					cv.currPieceOnTouch = 13;
				} else if (newy < bitmapHd5 * 4) {
					cv.currPieceOnTouch = 18;
				} else {
					cv.currPieceOnTouch = 23;
				}
			} else {
				if (newy < bitmapHd5) {
					cv.currPieceOnTouch = 4;
				} else if (newy < bitmapHd5 * 2) {
					cv.currPieceOnTouch = 9;
				} else if (newy < bitmapHd5 * 3) {
					cv.currPieceOnTouch = 14;
				} else if (newy < bitmapHd5 * 4) {
					cv.currPieceOnTouch = 19;
				} else {
					cv.currPieceOnTouch = 24;
				}
			}
		}

		if (event.getAction() == MotionEvent.ACTION_UP) {
			cv.movingPiece = false;
			if (newx < bitmapWd5) {
				if (newy < bitmapHd5) {
					cv.currSlotOnTouchUp = 0;
				} else if (newy < bitmapHd5 * 2) {
					cv.currSlotOnTouchUp = 5;
				} else if (newy < bitmapHd5 * 3) {
					cv.currSlotOnTouchUp = 10;
				} else if (newy < bitmapHd5 * 4) {
					cv.currSlotOnTouchUp = 15;
				} else {
					cv.currSlotOnTouchUp = 20;
				}
			} else if (newx < bitmapWd5 * 2) {
				if (newy < bitmapHd5) {
					cv.currSlotOnTouchUp = 1;
				} else if (newy < bitmapHd5 * 2) {
					cv.currSlotOnTouchUp = 6;
				} else if (newy < bitmapHd5 * 3) {
					cv.currSlotOnTouchUp = 11;
				} else if (newy < bitmapHd5 * 4) {
					cv.currSlotOnTouchUp = 16;
				} else {
					cv.currSlotOnTouchUp = 21;
				}
			} else if (newx < bitmapWd5 * 3) {
				if (newy < bitmapHd5) {
					cv.currSlotOnTouchUp = 2;
				} else if (newy < bitmapHd5 * 2) {
					cv.currSlotOnTouchUp = 7;
				} else if (newy < bitmapHd5 * 3) {
					cv.currSlotOnTouchUp = 12;
				} else if (newy < bitmapHd5 * 4) {
					cv.currSlotOnTouchUp = 17;
				} else {
					cv.currSlotOnTouchUp = 22;
				}
			} else if (newx < bitmapWd5 * 4) {
				if (newy < bitmapHd5) {
					cv.currSlotOnTouchUp = 3;
				} else if (newy < bitmapHd5 * 2) {
					cv.currSlotOnTouchUp = 8;
				} else if (newy < bitmapHd5 * 3) {
					cv.currSlotOnTouchUp = 13;
				} else if (newy < bitmapHd5 * 4) {
					cv.currSlotOnTouchUp = 18;
				} else {
					cv.currSlotOnTouchUp = 23;
				}
			} else {
				if (newy < bitmapHd5) {
					cv.currSlotOnTouchUp = 4;
				} else if (newy < bitmapHd5 * 2) {
					cv.currSlotOnTouchUp = 9;
				} else if (newy < bitmapHd5 * 3) {
					cv.currSlotOnTouchUp = 14;
				} else if (newy < bitmapHd5 * 4) {
					cv.currSlotOnTouchUp = 19;
				} else {
					cv.currSlotOnTouchUp = 24;
				}
			}

			if (cv.currPieceOnTouch != cv.currSlotOnTouchUp) {
				cv.sendPieceToNewSlot(cv.currPieceOnTouch, cv.currSlotOnTouchUp);
				cv.playSetSound();
			} else {
				cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px = cv.puzzleSlots[cv.currSlotOnTouchUp].sx;
				cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py = cv.puzzleSlots[cv.currSlotOnTouchUp].sy;
			}
		}

		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			cv.movingPiece = true;
			if (cv.currPieceOnTouch >= 0 || cv.currPieceOnTouch <= PIECES - 1) {
				cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px = newx
						- bitmapWd5 / 2;
				cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py = newy
						- bitmapHd5 / 2;
			}
		}

		cv.inPlace = 0;
		for (int i = 0; i < cv.numberOfPieces; i++) {
			if (cv.puzzleSlots[i].slotNum == cv.puzzleSlots[i].puzzlePiece.pieceNum) {
				cv.inPlace++;
			}
		}

		if (cv.inPlace == cv.numberOfPieces) {
			stopTimer();
			cv.solved = true;
			if (!cv.mSaveButton.isShown())
				cv.mSaveButton.setVisibility(View.VISIBLE);
			if (!cv.devArtButton.isShown())
				cv.devArtButton.setVisibility(View.VISIBLE);
			if (!cv.pixivButton.isShown())
				cv.pixivButton.setVisibility(View.VISIBLE);
			cv.thread.setState(PuzzleSurface.STATE_WIN);
			return false;
		}

		return true;
	}

	@Override
	public void recylceAll() {
		if (cv.image != null)
			cv.image.recycle();

		for (int i = 0; i < cv.puzzlePieces.length; i++)
			if (cv.puzzlePieces != null)
				if (cv.puzzlePieces[i] != null)
					if (cv.puzzlePieces[i].bitmap != null)
						cv.puzzlePieces[i].bitmap.recycle();

	}

	@Override
	public String getPercentComplete() {
		return "" + (float) piecesComplete / PIECES;
	}

	@Override
	public void initTimer() {
		startPuzzle = new Date();
	}

	@Override
	public void stopTimer() {
		stopPuzzle = new Date();
		currPuzzleTime += stopPuzzle.getTime() - startPuzzle.getTime();
	}

	@Override
	public void resetTimer() {
		currPuzzleTime = 0;
		startPuzzle = new Date();
	}

	@Override
	public double getSolveTime() {
		return currPuzzleTime / 1000.0;
	}

	@Override
	public int getCurrentImage() {
		// return current image index
		return index;
	}

	@Override
	public void getPrevousImageLoadedScaledDivided(Thread thread) {
		// TODO Auto-generated method stub
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {
				while (!newImageComplete) {
					// get new index value and then remove index
					index = cv.currentImage;

					cv.image = cv.decodeSampledBitmapFromResource(cv.res,
							Data.pics[cv.currentImage], cv.screenW, cv.screenH);

					cv.image = Bitmap.createScaledBitmap(cv.image, cv.screenW,
							cv.screenH, true);

					newImageComplete = divideBitmapFromPreviousPuzzle();

					if (newImageComplete) {
						resetTimer();
						cv.errorLoading = false;
						cv.solved = false;
						cv.imageReady = true;
					} else {
						cv.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}
}